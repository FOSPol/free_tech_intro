# What You Need to Know to Join the Alt-Tech Exodus

So, you've heard that big tech companies have transcended the niggling free market and reached the lofty heights of partisan politics --- and act accordingly.
[Unlike prominent political figures](https://reason.com/2021/01/11/ron-paul-says-hes-been-locked-out-of-facebook/) you might not make the top of the purge list, but it's no secret that censorship is surging.
Both to protect your freedom, and to stop providing resources to those who would undermine it, it makes sense to jump ship.
Now, when everybody else is also doing it.
But how?

## Freedom is a Skill

In the world of technology the reeds are tall, and you only get as much freedom as your choices afford you.
The taint of political virtue signalling has sadly not left the liberty-minded untouched, and there's plenty of snake-oil salesmen looking for your business.
So it's paramount to know how censorship can reliably be stymied.

## Freedom by Design Beats Freedom by Convention

Have you heard of this-or-that new platform whose board has “a strong commitment to free speech”?
Well, wouldn't you know it, so did  “big tech” until there was power to be gained by doing the opposite.
A more “virtuous” platform, or a partisan platform “from your side” will not sustainably support your liberty.
A platform *designed* with freedom in mind will.

## The Core Concept of Free Design Is Decentralization Over Centralization

There are two tiers for decentralized design.
The first and highest standard is p2p (peer-to-peer), which connects users directly and relies on no centralized infrastructure.
Sadly, not all content can feasibly be shared p2p (yet), so --- in a wonderful analogy to the offline world --- there is a second tier, federation.
Federated services use a common protocol via which anybody can distribute content as long as they have the resources to maintain a server.
Everybody could technically run their own server, but more often, a few hundred people maintain servers via which they make access to the protocol easily available to casual users.
The emergent framework prevents influence (in the form of a large user base) from coagulating in one instance.
Users can easily switch to a new provider (often keeping their data) and competition weeds out abusive providers.

## Tell me What to Use Already

No.
The best I can do is share with you the choices I made and provide an example rationale in light of the common priority of sustainable free speech.
Your choices might be different.


## Text Messaging

Text messaging fortunately is very lightweight and p2p approaches are feasible.
[Tox](https://tox.chat/) is a protocol which allows you and all of your contacts to chat based *solely* on an open-source client which you can install on your device.
There is no middleman who can censor you, no [centralized server that can be taken down by malicious actors](https://techcrunch.com/2021/01/11/parler-is-officially-offline-after-aws-suspension/), and no one company which can go bankrupt or have a change of heart — regarding [this whole free speech thing](https://www.nytimes.com/2021/01/06/opinion/trump-lies-free-speech.html).
Yes, it also supports video and audio chat.

### The way this works:
You [simply pick a client](https://tox.chat/clients.html), install it, get an ID, share it with someone, and you're good.
For ever.

## Social Media

This is a bit more tricky, as the content requirements do not lend themselves to p2p.
The biggest federated social network is known as the [“fediverse”](https://en.wikipedia.org/wiki/Fediverse).
You might never have heard of it, but you might have heard of software which uses the protocol (i.e. what an admin would run on his “instance”, i.e. his server), such as Pleroma or Mastodon.
Alternatively, you might have heard of the biggest instance to have used the fediverse to date, [Gab](https://finance.yahoo.com/news/gab-decentralizing-raising-10-million-134916662.html).
That sadly doesn't cover it all, since the choice of an instance remains very important.
To protect users from spam, or unwanted content (such as pornography) on their feed, the protocol allows instance administrators to block other instances, i.e. refuse to federate with them.
While this fulfills the stated purpose, many admins also [choose to block “problematic” instances](https://social.tchncs.de/@Chymera/102703795606574095).
Instances generally publish a block list, though.
You should check the “about” page of an instance you're considering either for a statement that they don't block any other instance, or to ascertain that the block list is compatible with your preferences.
I am looking to host my own instance, but barring that, I am considering [FSE](https://freespeechextremist.com/) and [liberdon](https://liberdon.com/) --- good free-speech instances which maintain no block list.
You can also give an instance a try on a whim and switch later, though.

### The way this works:
Find an instance you like (via web search), and register an account.


## Video Content

Creators who have been banned or demonetized by YouTube have congregated around a host of alternative platforms.
Most of these platforms, however, are simply imitations of the YouTube model, often with cryptocurrency tacked on for futuristic appeal.
There exists however a federated video-sharing protocol, [PeerTube](https://joinpeertube.org/instances).
Much of what was said about the fediverse applies here as well, other than it being less common for PeerTube instances to federate with everybody.
I enjoy using [QOTO](https://video.qoto.org/videos/local) and there are even a few search engines which look up all instances for content (e.g. [peertube-index](https://peertube-index.net/) and [sepiasearch](https://sepiasearch.org/)).

### The way this works:
As a content consumer, you can use one of the aforementioned search engines to look for content you want to watch.
No need to even register unless you want to keep track of favourites on somebody else's server.
If so, you can use web search or [this page](https://joinpeertube.org/instances) to find interesting instances.
Ideally, check how many instances they “follow” (more means access to more content) or how many total videos they give you access to (you can rank instances by either of these metrics [here](https://instances.joinpeertube.org/instances)), and you're done.

## Web Search

Believe it or not, but there is such a thing as p2p web search: [YaCy](https://yacy.net/).
YaCy is fairly easy to run on your own personal computer, and does not require you to maintain a server.
In a sort of combination between a p2p or federated model, you can run your own YaCy (which is the recommended approach), or use somebody else's in as far as they make it available (e.g. [noisytoot](https://yacy.noisytoot.org)).
Your mileage may vary, and sadly it will never be as fast as centralized web search, though it could be similarly fast if you install it on your own computer and have a good internet connection.

### The way this works:
You can just [download and install](https://yacy.net/download_installation/) YaCy, and you're set up to search the web on your own.
There are very very many configuration options, but you don't have to use them.

## Email

Email is already federated, as it turns out.
This might also help you grasp the concept of federation.
That the text following “@” differs from contact to contact, is a dead give-away for federation (social media accounts look similar on the fediverse).
That most people still go for large-scale providers with questionable involvements in non-market processes, is simply a symptom of good marketing and herd mentality.
Many email providers will offer you more censorship-robust services.
I particularly enjoy smaller providers, as they tend to have low running costs and can actually finance the service fully through donations.
Further, you can host your own email server, as I do.

### The way this works:
If you are concerned about your current provider, you can simply run a web-search for alternative providers, the process is much the same.

## Some of these options look daunting

Apathy and comfort are the enemies of freedom and very good bait to incentivize you to give it up.
After all, that's how we got here.
On a more gentle note, you don't have to go for all of the above, but if there is one particular service — such as social media or messaging — where you feel the cold breath of censorship down your back, it would be a good idea to give that a very serious try right now.
None of these options require any particular expertise in technology, though, so it's rather a question of stepping out of your comfort zone than of studying anything at any great length.
Not least of all, people are happy to help.
Most of these technologies have support chats linked on their websites, and their support is considerably better than what you might have become used to from your current provider's help desk.
Though it may not be overt of partisan, the spirit of freedom runs deep in the world of alt-tech, and people are more interested in empowering human action, than in corralling you into their service.
Be weary of “alternatives” which behave otherwise ;)

