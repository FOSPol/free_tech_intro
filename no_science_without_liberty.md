# Bureaucratization of Research

# Scientism Erodes the Neutrality of Science

Scientism, which here is understood as the promotion of science as the best or only objective means by which society should determine normative and epistemological values, is increasingly manifest in common discourse, and is, unlike other issues even more pervasive in the natural sciences than elsewhere.
%%% correct the above, it's not really pervasive in the natural sciences, but when they can be tied in to some normative quesion. vessel somehow

The erosion of neutrality brings with it an erosion of trust, which is manifest in the emergent anti-vaccination community (which is notably distinct from the anti-mandaotry-vaccination position), and most prominently flat earthers.
Far from indicating that free humans --- who have not been adequately coerced or propagandized into obeying experts --- will go awry, blanket and obstinate rejection of scientific insight is an indictment of the results of scientist propaganda.
People can very easily tell when they are being coerced, and some might be less refined in how they fight back.
Ridicule is hardly the appropriate answer, and certainly not from those who purport to be paragons of insight.
Rejecting the entire scientific establisment in circumstances as the current ones, is by no means a surprising response.

To put it into terms very dear to the cult of scientism, scientists who try to derive coercive authority from their insight, or empower the coercive authority of others with their credibility, impose a negative externality on the credibility of all other scientis, and a negative externality of all who might otherwise been able to consume scientific insight with little fear of political bias.
